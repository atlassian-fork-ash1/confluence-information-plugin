package com.atlassian.confluence.extra.information;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Streamable;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.macro.StreamableMacro;
import com.atlassian.confluence.macro.StreamableMacroAdapter;
import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.WysiwygBodyType;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;

import java.util.Map;

import static com.atlassian.confluence.macro.Macro.BodyType.RICH_TEXT;
import static com.atlassian.confluence.macro.Macro.OutputType.BLOCK;
import static com.atlassian.renderer.v2.RenderMode.ALL;
import static com.atlassian.renderer.v2.macro.WysiwygBodyType.WIKI_MARKUP;
import static com.google.common.collect.Maps.newHashMap;
import static java.lang.Boolean.parseBoolean;
import static org.apache.commons.lang3.StringUtils.isBlank;

public abstract class AbstractInformationMacro extends BaseMacro implements StreamableMacro {
    private final PageBuilderService pageBuilderService;
    private final TemplateRenderer templateRenderer;

    AbstractInformationMacro(PageBuilderService pageBuilderService, final TemplateRenderer templateRenderer) {
        this.pageBuilderService = pageBuilderService;
        this.templateRenderer = templateRenderer;
    }

    @Override
    public Streamable executeToStream(final Map<String, String> parameters, final Streamable body, final ConversionContext conversionContext) {
        includeRequiresResources(conversionContext);

        final Map<String, Object> data = newHashMap();
        data.put("title", parameters.get("title"));
        data.put("useIcon", isUseIcon(parameters));
        data.put("class", getCssClass());
        data.put("auiMessageClass", getAuiMessageClass());
        data.put("auiIconClass", getAuiIconClass());

        return writer -> {
            // Because Soy isn't stream-friendly, we need to break up the template into "before" and "after" fragments
            // in order to efficiently stream the macro body (otherwise we'd need to drain it into a string and inject
            // it into the soy data model, which would suck).
            // Note that the macro body has already been sanitized, so Soy's auto-escaping is unnecessary and undesirable
            // here - we can just stream directly to the output.
            templateRenderer.renderTo(writer, "confluence.extra.information:soy-templates", "Confluence.InformationMacro.before.soy", data);
            body.writeTo(writer);
            templateRenderer.renderTo(writer, "confluence.extra.information:soy-templates", "Confluence.InformationMacro.after.soy", data);
        };
    }

    private void includeRequiresResources(final ConversionContext conversionContext) {
        final RequiredResources requiredResources = pageBuilderService.assembler().resources();
        requiredResources.requireWebResource("confluence.extra.information:information-plugin-adg-styles");

        if ("mobile".equals(conversionContext.getOutputDeviceType())) {
            requiredResources.requireWebResource("confluence.extra.information:information-plugin-mobile-styles");
        }
    }

    @Override
    public String execute(final Map<String, String> map, final String s, final ConversionContext conversionContext)
            throws MacroExecutionException {
        return StreamableMacroAdapter.executeFromStream(this, map, s, conversionContext);
    }

    private static boolean isUseIcon(final Map<String, String> parameters) {
        final String useIconParam = parameters.get("icon");
        return isBlank(useIconParam) || parseBoolean(useIconParam);
    }

    @Override
    public BodyType getBodyType() {
        return RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return BLOCK;
    }

    protected abstract String getCssClass();

    protected abstract String getAuiMessageClass();

    protected abstract String getAuiIconClass();

    /**
     * ----------------------------------- Old v2 base macro methods -----------------------------------
     */
    @SuppressWarnings("unchecked")
    @Override
    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
        try {
            return execute(parameters, body, new DefaultConversionContext(renderContext));
        } catch (MacroExecutionException e) {
            throw new MacroException(e);
        }
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return ALL;
    }

    /**
     * Info Panels contain wiki markup and we want it to be rendered in the wysiwyg editor so that the formatted text is
     * shown richly.
     *
     * @return WysiwygBodyType#WIKI_MARKUP
     */
    @Override
    public WysiwygBodyType getWysiwygBodyType() {
        return WIKI_MARKUP;
    }

    public boolean hasBody() {
        return true;
    }

    /**
     * Although {@link #getTokenType} supersedes this method, for backwards compatibility with Confluence 3.0 and before
     * we need to override isInline too.
     *
     * @return false
     */
    @Override
    public boolean isInline() {
        return false;
    }

    /**
     * Info panels are always blocks.
     *
     * @return TokenType#BLOCK
     */
    @Override
    public TokenType getTokenType(Map map, String s, RenderContext renderContext) {
        return TokenType.BLOCK;
    }
}
