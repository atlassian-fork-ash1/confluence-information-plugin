package com.atlassian.confluence.extra.information;

import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class TipMacro  extends AbstractInformationMacro
{
    public TipMacro(PageBuilderService pageBuilderService, TemplateRenderer templateRenderer)
    {
        super(pageBuilderService, templateRenderer);
    }

    @Override
    protected String getCssClass()
    {
        return "confluence-information-macro-tip";
    }

    @Override
    protected String getAuiMessageClass()
    {
        return "aui-message-success";
    }

    @Override
    protected String getAuiIconClass()
    {
        return "aui-iconfont-approve";
    }
}
