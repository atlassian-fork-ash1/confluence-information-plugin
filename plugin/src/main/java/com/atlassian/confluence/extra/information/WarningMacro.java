package com.atlassian.confluence.extra.information;

import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class WarningMacro extends AbstractInformationMacro
{
    public WarningMacro(PageBuilderService pageBuilderService, TemplateRenderer templateRenderer)
    {
        super(pageBuilderService, templateRenderer);
    }

    @Override
    protected String getCssClass()
    {
        return "confluence-information-macro-warning";
    }

    @Override
    protected String getAuiMessageClass()
    {
        return "aui-message-error";
    }

    @Override
    protected String getAuiIconClass()
    {
        return "aui-iconfont-error";
    }
}
