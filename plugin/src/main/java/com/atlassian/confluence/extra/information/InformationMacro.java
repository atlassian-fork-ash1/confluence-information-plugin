package com.atlassian.confluence.extra.information;

import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;

public class InformationMacro extends AbstractInformationMacro
{
    public InformationMacro(PageBuilderService pageBuilderService, TemplateRenderer templateRenderer)
    {
        super(pageBuilderService, templateRenderer);
    }

    @Override
    protected String getCssClass()
    {
        return "confluence-information-macro-information";
    }

    @Override
    protected String getAuiMessageClass()
    {
        return "aui-message-hint";
    }

    @Override
    protected String getAuiIconClass()
    {
        return "aui-iconfont-info";
    }
}
